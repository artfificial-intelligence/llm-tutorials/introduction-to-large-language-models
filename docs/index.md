# Large Language Models 개요<sup>[1](#footnote_1)</sup>

> <font size="3">대형 언어 모델(LLM, Large Language Models)이 무엇인지, 왜 중요한지 알아본다.</font>

## 목차

1. [LLM이란?](./intro.md#intro)
1. [LLM을 어떻게 훈련하는가?](./intro.md#sec_02)
1. [LLM의 어플리케이션](./intro.md#sec_03)
1. [LLM의 과제와 위험](./intro.md#sec_04)
1. [LLM의 미래 방향](./intro.md#summary)

<a name="footnote_1">1</a>: [LLM Tutorial 1 — Introduction to Large Language Models](https://medium.com/ai-advances/llm-tutorial-1-introduction-to-large-language-models-b6799355ef1c?sk=c089871e220cbc8a5bac4696b78c3be4)를 편역핬습니다.
